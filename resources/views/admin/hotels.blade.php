@extends('layouts.admin_template')

@section('content')

	<h2>Hotels List</h2>

	<div class="col">
		<table class="table">
			<thead>
				<tr>
				<th>Hotel Name</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
			</thead>
			<tbody>
				<tr>
					<td><a href="#" data-toggle="modal" data-target="#hotelModal"> A Hotel </a></td>
					<td><a href="#" class="btn btn-warning"> Edit</a></td>
					<td><a href="#" class="btn btn-danger"> Delete</a></td>
				</tr>
				<tr>
					<td><a href="#" data-toggle="modal" data-target="#hotelModal"> B Hotel </a></td>
					<td><a href="#" class="btn btn-warning"> Edit</a></td>
					<td><a href="#" class="btn btn-danger"> Delete</a></td>
				</tr>
				<tr>
					<td><a href="#" data-toggle="modal" data-target="#hotelModal">  C Hotel </a></td>
					<td><a href="#" class="btn btn-warning"> Edit</a></td>
					<td><a href="#" class="btn btn-danger"> Delete</a></td>
				</tr>
			</tbody>
		</table>
	</div>

@endsection