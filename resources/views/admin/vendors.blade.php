@extends('layouts.admin_template')

@section('content')
	
	<h2>Vendors List</h2>

	<div class="col">
		<table class="table">
			<thead>
				<tr>
				<th>Vendor Type</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
			</thead>
			<tbody>
				<tr>
					<td><a href="/backend/vendors/hotels"> Hotels </a></td>
					<td><a href="#" class="btn btn-warning"> Edit</a></td>
					<td><a href="#" class="btn btn-danger"> Delete</a></td>
				</tr>
				<tr>
					<td><a href="/backend/vendors/transport"> Tranports </a></td>
					<td><a href="#" class="btn btn-warning"> Edit</a></td>
					<td><a href="#" class="btn btn-danger"> Delete</a></td>
				</tr>
				<tr>
					<td><a href="/backend/vendors/agencies"> Agencies </a></td>
					<td><a href="#" class="btn btn-warning"> Edit</a></td>
					<td><a href="#" class="btn btn-danger"> Delete</a></td>
				</tr>
			</tbody>
		</table>
	</div>

@endsection