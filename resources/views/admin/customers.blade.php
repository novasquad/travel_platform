@extends('layouts.admin_template')

@section('content')
<div class="col-md-12 mx-auto">
	<h2>Customer List</h2>
	<table class="table" border="1">
		<thead>
			<tr>
				<th>No.</th>
				<th>Name</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>1</td>
				<td><a href="#" data-toggle="modal" data-target="#customerModal" >Cho Thet Oo</a></td>
				<td><a href="/backend/customers/edit/id" class="btn btn-warning">Edit</a></td>
				<td><a href="/backend/customers/delete/id" class="btn btn-danger">Delete</a></td>
			</tr>
			<tr>
				<td>2</td>
				<td><a href="#" data-toggle="modal" data-target="#customerModal" >Peter Lay</a></td>
				<td><a href="/backend/customers/edit/id" class="btn btn-warning">Edit</a></td>
				<td><a href="/backend/customers/delete/id" class="btn btn-danger">Delete</a></td>
			</tr>
		</tbody>
	</table>
</div>

@endsection