<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/backend', function(){
	return view('admin.admin_home', [
		'title' => 'Admin Panel',
		'breadcrumb_name' => 'Main'
	]);
});

Route::get('/backend/customers',function(){
	return view('admin.customers',[
		'title'=>'Customers',
		'breadcrumb_name'=>'Customers'
	]);

});

Route::get('/backend/vendors', function(){
	return view('admin.vendors', [
		'title' => 'Vendors Panel',
		'breadcrumb_name' => 'Vendors'
	]);
});

Route::get('/backend/vendors/hotels', function (){
	return view('admin.hotels',[
		'title' => 'Hotels Panel',
		'breadcrumb_name' => 'Hotels'
	]);
});
